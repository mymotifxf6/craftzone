/*
SQLyog Enterprise v12.09 (64 bit)
MySQL - 8.0.18 : Database - craftzone
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `customers` */

CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(45) COLLATE utf8mb4_general_ci NOT NULL,
  `mobile` varchar(11) COLLATE utf8mb4_general_ci NOT NULL,
  `password` text COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `customers` */

insert  into `customers`(`id`,`name`,`email`,`mobile`,`password`) values (1,'Abigail Jane','abigailjane@hotmail.com','9827152619','asdasdqweqwe');
insert  into `customers`(`id`,`name`,`email`,`mobile`,`password`) values (2,'Gabrielle Dorothy','gabrielledorothy_yahoo.com','8261826301','ertertyertyertfghdfgh');
insert  into `customers`(`id`,`name`,`email`,`mobile`,`password`) values (3,'Elizabeth Heather','elizabethheather@yahoo.com','9872517264','asdqwqfgfghfghvssdf');
insert  into `customers`(`id`,`name`,`email`,`mobile`,`password`) values (4,'Emma Watson','emmawatson@hotmail.com','9261400876','sdasdasdzxcxcf');

/*Table structure for table `product_categories` */

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `product_categories` */

insert  into `product_categories`(`id`,`title`,`status`) values (1,'Demon Masks',1);
insert  into `product_categories`(`id`,`title`,`status`) values (2,'Durga Masks',1);
insert  into `product_categories`(`id`,`title`,`status`) values (3,'Hanging Bags',1);
insert  into `product_categories`(`id`,`title`,`status`) values (4,'Decorative Glass',1);
insert  into `product_categories`(`id`,`title`,`status`) values (5,'Religious Posters',1);
insert  into `product_categories`(`id`,`title`,`status`) values (6,'Posters',1);

/*Table structure for table `product_reviews` */

CREATE TABLE `product_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `message` text COLLATE utf8mb4_general_ci,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ForeignKeyCustomer` (`customer_id`),
  KEY `ForeignKeyProduct` (`product_id`),
  CONSTRAINT `ForeignKeyProduct` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `product_reviews` */

insert  into `product_reviews`(`id`,`customer_id`,`product_id`,`rating`,`message`,`createdAt`,`updatedAt`) values (1,1,3,2.3,'Cheap product','2021-09-24 22:45:22','2021-09-24 22:45:26');
insert  into `product_reviews`(`id`,`customer_id`,`product_id`,`rating`,`message`,`createdAt`,`updatedAt`) values (2,2,4,4,'Nice build quality','2021-08-20 22:47:23','2021-08-08 22:47:29');
insert  into `product_reviews`(`id`,`customer_id`,`product_id`,`rating`,`message`,`createdAt`,`updatedAt`) values (3,NULL,1,3.5,'Simple plastic could have been better','2021-08-12 22:47:39','2021-08-12 22:47:44');
insert  into `product_reviews`(`id`,`customer_id`,`product_id`,`rating`,`message`,`createdAt`,`updatedAt`) values (4,NULL,2,2.7,NULL,'2021-06-09 22:47:48','2021-06-09 22:47:53');
insert  into `product_reviews`(`id`,`customer_id`,`product_id`,`rating`,`message`,`createdAt`,`updatedAt`) values (5,3,5,4.5,'Nice packaging','2021-04-16 22:47:59','2021-04-16 22:47:59');
insert  into `product_reviews`(`id`,`customer_id`,`product_id`,`rating`,`message`,`createdAt`,`updatedAt`) values (6,2,5,4.2,'Can find much cheaper in outlet store','2021-06-24 22:48:09','2021-06-24 22:48:09');
insert  into `product_reviews`(`id`,`customer_id`,`product_id`,`rating`,`message`,`createdAt`,`updatedAt`) values (7,4,5,3.9,'Too expensive','2021-06-19 22:48:15','2021-06-19 22:48:15');
insert  into `product_reviews`(`id`,`customer_id`,`product_id`,`rating`,`message`,`createdAt`,`updatedAt`) values (8,NULL,8,3.1,NULL,'2021-07-17 22:48:22','2021-07-17 22:48:22');
insert  into `product_reviews`(`id`,`customer_id`,`product_id`,`rating`,`message`,`createdAt`,`updatedAt`) values (9,NULL,1,2.9,NULL,'2021-09-04 22:48:36','2021-09-04 22:48:36');
insert  into `product_reviews`(`id`,`customer_id`,`product_id`,`rating`,`message`,`createdAt`,`updatedAt`) values (10,NULL,9,4.1,NULL,'2021-09-06 22:48:41','2021-09-06 22:48:41');
insert  into `product_reviews`(`id`,`customer_id`,`product_id`,`rating`,`message`,`createdAt`,`updatedAt`) values (11,1,5,2.7,NULL,NULL,NULL);

/*Table structure for table `products` */

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `categoryid` tinyint(1) DEFAULT NULL,
  `price` float NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `image` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `ratings` float DEFAULT NULL,
  `reviews` int(11) DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`sku`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `products` */

insert  into `products`(`id`,`sku`,`title`,`description`,`categoryid`,`price`,`quantity`,`image`,`ratings`,`reviews`,`createdAt`,`updatedAt`) values (1,'PSTK004','Ma Kali Tarapith','A religious poster reprint on paper - unframed (8 x 11 inches). The precise color of the item may vary depending on the specific monitor, the settings and the lighting conditions. Will be delivered exactly what the image displays.',6,500,1,'https://i.postimg.cc/x8XZzbgp/PSTK004.jpg',3.2,2,NULL,NULL);
insert  into `products`(`id`,`sku`,`title`,`description`,`categoryid`,`price`,`quantity`,`image`,`ratings`,`reviews`,`createdAt`,`updatedAt`) values (2,'PSTK002','Ma Kali Glittering','A religious poster reprint on paper - unframed (8 x 11 inches). The precise color of the item may vary depending on the specific monitor, the settings and the lighting conditions. Will be delivered exactly what the image displays.',6,550,1,'https://i.postimg.cc/X7yhNdrp/PSTK002.jpg',2.7,1,NULL,NULL);
insert  into `products`(`id`,`sku`,`title`,`description`,`categoryid`,`price`,`quantity`,`image`,`ratings`,`reviews`,`createdAt`,`updatedAt`) values (3,'PSTK003','Ramakrishna & Ma Sarada with Ma Kali','A religious poster reprint on paper - unframed (8 x 11 inches). The precise color of the item may vary depending on the specific monitor, the settings and the lighting conditions. Will be delivered exactly what the image displays.',6,600,2,'https://i.postimg.cc/Gpj6wRHK/PSTK003.jpg',2.3,1,NULL,NULL);
insert  into `products`(`id`,`sku`,`title`,`description`,`categoryid`,`price`,`quantity`,`image`,`ratings`,`reviews`,`createdAt`,`updatedAt`) values (4,'SKDECOR49','Ma Sherawali','A religious poster reprint on paper - unframed (8 x 11 inches). The precise color of the item may vary depending on the specific monitor, the settings and the lighting conditions. Will be delivered exactly what the image displays.',6,650,5,'https://i.postimg.cc/7Z8R5hzX/SKDECOR49.jpg',4,1,NULL,NULL);
insert  into `products`(`id`,`sku`,`title`,`description`,`categoryid`,`price`,`quantity`,`image`,`ratings`,`reviews`,`createdAt`,`updatedAt`) values (5,'PSM013','Lord Ganesha Glittering','A religious poster reprint on paper - unframed (8 x 11 inches). The precise color of the item may vary depending on the specific monitor, the settings and the lighting conditions. Will be delivered exactly what the image displays.',6,700,1,'https://i.postimg.cc/bNDXG4BY/PSM013.jpg',3.8,4,NULL,NULL);
insert  into `products`(`id`,`sku`,`title`,`description`,`categoryid`,`price`,`quantity`,`image`,`ratings`,`reviews`,`createdAt`,`updatedAt`) values (6,'PSM015','Radha Krishna on Swing','A religious poster reprint on paper - unframed (8 x 11 inches). The precise color of the item may vary depending on the specific monitor, the settings and the lighting conditions. Will be delivered exactly what the image displays.',6,750,4,'https://i.postimg.cc/7YpFSXF3/PSM015.jpg',NULL,NULL,NULL,NULL);
insert  into `products`(`id`,`sku`,`title`,`description`,`categoryid`,`price`,`quantity`,`image`,`ratings`,`reviews`,`createdAt`,`updatedAt`) values (7,'PSM019','Lord Ganesha','A religious poster reprint on paper - unframed (8 x 11 inches). The precise color of the item may vary depending on the specific monitor, the settings and the lighting conditions. Will be delivered exactly what the image displays.',6,800,1,'https://i.postimg.cc/wMMCQrxV/PSM019.jpg',NULL,NULL,NULL,NULL);
insert  into `products`(`id`,`sku`,`title`,`description`,`categoryid`,`price`,`quantity`,`image`,`ratings`,`reviews`,`createdAt`,`updatedAt`) values (8,'PSM020','Lord Ganesha','A religious poster reprint on paper - unframed (8 x 11 inches). The precise color of the item may vary depending on the specific monitor, the settings and the lighting conditions. Will be delivered exactly what the image displays.',6,850,3,'https://i.postimg.cc/tJcLSzYH/PSM020.jpg',3.1,1,NULL,NULL);
insert  into `products`(`id`,`sku`,`title`,`description`,`categoryid`,`price`,`quantity`,`image`,`ratings`,`reviews`,`createdAt`,`updatedAt`) values (9,'PSM026','Ma Sherawali with her nine avatars','A religious poster reprint on paper - unframed (8 x 11 inches). The precise color of the item may vary depending on the specific monitor, the settings and the lighting conditions. Will be delivered exactly what the image displays.',6,900,1,'https://i.postimg.cc/vZfphfyp/PSM026.jpg',4.1,1,NULL,NULL);
insert  into `products`(`id`,`sku`,`title`,`description`,`categoryid`,`price`,`quantity`,`image`,`ratings`,`reviews`,`createdAt`,`updatedAt`) values (10,'SKDECOR52','Ma Sherawali with her nine avatars','A religious poster reprint on paper - unframed (8 x 11 inches). The precise color of the item may vary depending on the specific monitor, the settings and the lighting conditions. Will be delivered exactly what the image displays.',6,1000,1,'https://i.postimg.cc/QNzwr0x2/SKDECOR52.jpg',NULL,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
