SELECT product_id, COUNT(rating) AS rating_num, FORMAT((SUM(rating)/COUNT(rating)),1) AS average_rating FROM product_reviews GROUP BY product_id

SELECT FORMAT((SUM(rating)/COUNT(rating)),1) AS ratings FROM product_reviews GROUP BY product_id

SELECT product_id, COUNT(rating) AS reviews FROM product_reviews GROUP BY product_id

UPDATE products p SET ratings = (SELECT FORMAT((SUM(rating)/COUNT(rating)),1) AS ratings FROM product_reviews WHERE p.id = product_id GROUP BY product_id), reviews = (SELECT COUNT(rating) AS reviews FROM product_reviews WHERE p.id = product_id GROUP BY product_id)

SELECT * FROM products