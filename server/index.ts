import { Server } from "./server";
require("dotenv").config();

let server = new Server().app;
const port = process.env.APP_PORT || 5000;
server.listen(port, () => {
    console.log("Server is running on port... " + port);
});