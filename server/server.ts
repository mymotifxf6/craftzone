require('dotenv').config();
import * as express from "express";
import MenuRouter from "./routes/MenuRouter";
import ItemRouter from "./routes/ItemRouter";
import OrderRouter from "./routes/OrderRouter";
import ProductRouter from "./routes/ProductRouter";
import CategoryRouter from "./routes/CategoryRouter";
import UserRouter from "./routes/UserRouter"

export class Server {
    public app: express.Application = express();

    constructor() {
        this.app.use(function (req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header(
                "Access-Control-Allow-Headers",
                "Origin, X-Requested-With, Content-Type, Accept"
            );
            next();
        });
        this.app.use(express.json());
        this.setRoutes();
        this.error404Handler();
        this.handleErrors();
    }

    setRoutes() {
        this.app.use("/api/menus", MenuRouter);
        this.app.use("/api/items", ItemRouter);
        this.app.use("/api/orders", OrderRouter);
        this.app.use("/api/products", ProductRouter);
        this.app.use("/api/categories", CategoryRouter);
        this.app.use("/api/users", UserRouter);
    }

    error404Handler() {
        this.app.use((req, res) => {
            res.status(404).json({
                message: "Not Found",
                status_code: 404,
            });
        });
    }

    handleErrors() {
        this.app.use((error, req, res, next) => {
            const errorStatus = req.errorStatus || 500;
            res.status(errorStatus).json({
                message: error.message || "Something went wrong, please try again",
                status_code: errorStatus,
            });
        });
    }
}