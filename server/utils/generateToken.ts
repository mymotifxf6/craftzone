require("dotenv").config();
import { sign } from "jsonwebtoken";

const generateToken = (data: any) => {
    return sign({ data }, process.env.TOKEN_KEY, {
        expiresIn: "1h",
    });
}

export default generateToken;