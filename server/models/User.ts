import pool from "../db/db";

class User {
    

    /** Fetching a single user */     
    static getUser = (data: any, callBack: any) => {
        const sql = `SELECT * FROM users WHERE email = ?`;
        pool.query(
            sql, [data], (err, results) => {
                if (err) { 
                    return callBack(err); 
                }
                return callBack(null, results);
            }
        )
    }

    /** Function to insert new customer */
    static createUser = (data: any, callBack: any) => {
        const usrExists = `SELECT count(*) AS usercount FROM users WHERE email = ?`;
        pool.query(
            usrExists, [data.email], (err, results) => {
                if (err) {
                    return callBack(err);
                }

                /** Check if user exists before insertion */
                if (results[0].usercount > 0) {
                    return callBack(null, 'User already exists');
                } else {
                    const sql = `INSERT INTO users(name, email, mobile, password) VALUES (?,?,?,?)`;
                    pool.query(
                        sql, [data.name, data.email, data.mobile, data.password], (err, results) => {
                            if (err) { 
                                return callBack(err); 
                            }
                            //return callBack(null, 'Registration successful');
                            pool.query(`SELECT name FROM users WHERE id=?`, [results.insertId], (err, result) => {
                                if (err) { 
                                    return callBack(err);
                                }
                                return callBack(null, result);
                            });
                        }
                    )
                }
            }
        )
    }
}

export default User;