import pool from "../db/db";

class Category {

    /** List all categories */
    static getCategories = (callBack: any) => {
        const sql = `SELECT * FROM product_categories`;
        pool.query(
            sql, [], (err, results) => {
                if (err) { return callBack(err); }
                return callBack(null, results);
            }
        )
    }
}

export default Category;