import pool from "../db/db";

class Order {
    /** List all orders */
    static getOrders = (callBack: any) => {
        const sql = `SELECT o.id, c.name, o.delivery, o.payment_method, o.total, o.created_at 
                        FROM users c, orders o  WHERE c.id = o.cust_id`;
        pool.query(
            sql, [], (err, results) => {
                if (err) { return callBack(err); }
                return callBack(null, results);
            }
        )
    }

    /** Fetch monthly sales */
    static getMonthlySales = (callBack: any) => {
        const sql = `SELECT
                        ROUND(SUM(o.total)) AS total_amount,
                        MONTHNAME(o.created_at) AS order_month,
                        YEAR(o.created_at) AS order_year
                        FROM
                        users c,
                        orders o
                        WHERE c.id = o.cust_id
                        GROUP BY order_month, order_year
                        ORDER BY order_year`;
        pool.query(
            sql, [], (err, results) => {
                if (err) { return callBack(err); }
                return callBack(null, results);
            }
        )
    }
}

export default Order;