import pool from "../db/db";

class Item {

    /** List all items */
    static getItems = (callBack: any) => {
        const sql = `SELECT m.title AS menu_title, i.title AS item_title, i.description, i.price
                        FROM items i INNER JOIN menu_item mt ON i.id = mt.item_id
                        INNER JOIN menus m ON mt.menu_id = m.id`;
        pool.query(
            sql, [], (err, results) => {
                if (err) { return callBack(err); }
                return callBack(null, results);
            }
        )
    }

    /** Creating Items and associating them with relevant menus */
    static createItem = (data: any, callBack: any) => {
        pool.beginTransaction((err) => {
            if (err) {
                throw err;
            }
            const sql = `INSERT INTO items(title, description, status, price) VALUES (?,?,?,?)`;
            const sql1 = `INSERT INTO menu_item(menu_id, item_id) VALUES (?,?)`;
            pool.query(sql, [data.title, data.description, data.status, data.price], (err, result) => {
                if (err) { 
                    return pool.rollback(() => {
                        callBack(err);
                    });
                }

                if (result.insertId > 0) {
                    pool.query(sql1, [data.menuid, result.insertId], (err, result) => {
                        if (err) { 
                            return pool.rollback(() => {
                                callBack(err);
                            });
                        }  
                        pool.commit((err) => {
                            if (err) { 
                                return pool.rollback(() => {
                                    callBack(err);
                                });
                            }
                            //pool.end();
                            return callBack(null, result);
                        });
                    });
                }
            })
        });
    }

    /** Fetching a single item */     
    static getItem = (id: Number, callBack: any) => {
        const sql = `SELECT m.title AS menu_title, i.title AS item_title, i.description, i.price
                      FROM items i INNER JOIN menu_item mt ON i.id = mt.item_id
                      INNER JOIN menus m ON mt.menu_id = m.id WHERE i.id = ?`;
        pool.query(
            sql, [id], (err, results) => {
                if (err) { return callBack(err); }
                return callBack(null, results);
            }
        )
    }

    /** Function to update the existing menu via id */
    static updateItem = (data: any, callBack: any) => {
        pool.beginTransaction((err) => {
            if (err) { throw err; }
            const sql = `UPDATE items SET title = ?, description = ?, status = ?, price = ? WHERE id = ?`;
            const sql1 = `UPDATE menu_item SET menu_id = ? WHERE item_id = ?`;
            pool.query(sql, [data.title, data.description, data.status, data.price, data.id], (err, result) => {
                if (err) { 
                    return pool.rollback(() => {
                        callBack(err);
                    });
                }
                pool.query(sql1, [data.menuid, data.id], (err, result) => {
                    if (err) { 
                        return pool.rollback(() => {
                            callBack(err);
                        });
                    }
                    pool.commit((err) => {
                        if (err) { 
                            return pool.rollback(() => {
                                callBack(err);
                            });
                        }                        
                        return callBack(null, result);
                    });
                });
            })
        });
    }
}

export default Item;