import pool from "../db/db";

class Product {

    /** List all items */
    static getProducts = (callBack: any) => {
        const sql = `SELECT * FROM products`;
        pool.query(
            sql, [], (err, results) => {
                if (err) { return callBack(err); }
                return callBack(null, results);
            }
        )
    }    

    /** Fetching a single product */     
    static getProduct = (id: number, callBack: any) => {
        const sql = `SELECT * FROM products WHERE id = ?`;
        pool.query(
            sql, [id], (err, results) => {
                if (err) { return callBack(err); }
                return callBack(null, results);
            }
        )
    }

    static getReviews = (id: any, callBack: any) => {
        const sql = `SELECT pr.message, pr.rating, c.name, pr.createdAt, pr.updatedAt FROM product_reviews pr, users c WHERE pr.message IS NOT NULL AND pr.customer_id=c.id AND product_id = ?`;
        pool.query(
            sql, [id], (err, results) => {
                if (err) { return callBack(err); }
                return callBack(null, results);
            }
        )
    }
}

export default Product;