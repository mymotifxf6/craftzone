import pool from "../db/db";

class Menu {
    
    /** Function to fetch all exiting menus */
    static getMenus = (offset: number, callBack: any) => {
        if(!offset) {
            offset = 0;
        }
        const sql = `SELECT * FROM posts LIMIT 10 OFFSET ${offset}`;
        pool.query(
            sql, [], (err, results) => {
                if (err) { return callBack(err); }
                return callBack(null, results);
            }
        )
    }

    /** Function to fetch a menu */
    static getMenu = (id: Number, callBack: any) => {
        const sql = `SELECT id, title, description, status, image FROM menus WHERE id = ?`;
        pool.query(
            sql, [id], (err, results) => {
                if (err) { return callBack(err); }
                return callBack(null, results);
            }
        )
    }

    /** Function to insert new menu */
    static createMenu = (data: any, callBack: any) => {
        const sql = `INSERT INTO menus(title, description, status) VALUES (?,?,?)`;
        pool.query(
            sql, [data.title, data.description, data.status], (err, results) => {
                if (err) { return callBack(err); }
                return callBack(null, results);
            }
        )
    }

    /** Function to update the existing menu via id */
    static updateMenu = (data: any, callBack: any) => {
        const sql = `UPDATE menus SET title = ?, description = ?, status = ? WHERE id = ?`;
        pool.query(
            sql, [data.title, data.description, data.status, data.id], (err, results) => {
                if (err) { return callBack(err); }
                return callBack(null, results);
            }
        )
    }

    /** Function to update the existing menu via id */
    static deleteMenu = (id: Number, callBack: any) => {
        const sql = `DELETE FROM menus WHERE id = ?`;
        pool.query(
            sql, [id], (err, results) => {
                if (err) { return callBack(err); }
                return callBack(null, results);
            }
        )
    }

}

export default Menu;