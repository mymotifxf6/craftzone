require("dotenv").config();
import Item from "../models/Item";

export default class ItemController {
    
    /** Function to fetch all items */
    static async getItems(req: any, res: any, next: any) {
        try {
            Item.getItems((err: any, results: any) => {
                if (err) { return next(err); }
                    return res.json({
                        success: 1,
                        data: results
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }

    /** Function to fetch a menu */
    static async getItem(req: any, res: any, next: any) {
        const id = req.params.id;
        try {
            Item.getItem(id, (err: any, results: any) => {
                    if (err) { return next(err); }
                    if (results.length === 0) {
                        return res.json({
                            success: 0,
                            message: "Item not found"
                        });
                    }
                    return res.json({
                        success: 1,
                        data: results
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }

    /** Function to create Item */
    static async createItem(req: any, res: any, next: any) {
        const body = req.body;
        try {
            Item.createItem(body, (error: any, results: any) => {
                    if (error) { return next(error); }
                    return res.json({
                        success: 1,
                        data: results
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }

    /** Function to update */
    static async updateItem(req: any, res: any, next: any) {
        const body = req.body;
        try {
            Item.updateItem(body, (error: any, results: any) => {
                if (error) { return next(error); }
                if (results.length === 0) {
                    return res.json({
                        success: 0,
                        message: "Failed to update item"
                    });
                }
                return res.json({
                    success: 1,
                    message: "Item data updated successfully"
                });
            });
        } catch (error) {
            next(error);
        }
    }

    // /** Function to delete */
    // static async deleteItem(req: any, res: any, next: any) {
    //     const id = req.params.id;
    //     try {
    //         Item.deleteItem(id, (error: any, results: any) => {
    //                 if (error) { return next(error); }
    //                 if (results.length === 0) {
    //                     return res.json({
    //                         success: 0,
    //                         message: "Item not found"
    //                     });
    //                 }
    //                 return res.json({
    //                     success: 1,
    //                     message: "Item removed successfully"
    //                 });
    //             }
    //         );
    //     } catch (error) {
    //         next(error);
    //     }
    // }
}