import { genSaltSync, hashSync, compareSync } from 'bcrypt';
import generateToken from '../utils/generateToken';
import User from "../models/User";

export default class UserController {

    /** Function to user info */
    static async getUser(req: any, res: any, next: any) {
        const body = req.body;       
        try {
            User.getUser(body.email, (err: any, results: any) => {
                    if (err) { return next(err); }
                    
                    if (results.length === 0) {
                        return res.json({
                            success: 0,
                            message: "Invalid email or password"
                        });
                    }

                    const pwdMatch = compareSync(body.password, results[0].password);
                    if(pwdMatch) {
                        delete results[0]['password'];
                        return res.json({ 
                            success: 1,
                            message: 'Login Successful',
                            data: results,
                            token: generateToken(results[0]),
                        });
                    } else {
                        return res.json({
                            success: 0,
                            message: "Invalid email or password"
                        });
                    }
                }
            );
        } catch (error) {
            next(error);
        }
    }

    /** Function to create User */
    static async createUser(req: any, res: any, next: any) {
        const body = req.body;
        const salt = genSaltSync(10);
        body.password = hashSync(body.password, salt);

        try {
            User.createUser(body, (error: any, results: any) => {
                    if (error) { return next(error); }
                    return res.json({
                        success: 1,
                        data: results
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }
}