require("dotenv").config();
import Order from "../models/Order";

export default class MenuController {
    
    /** Function to fetch all orders */
    static async getOrders(req: any, res: any, next: any) {
        try {
            Order.getOrders((err: any, results: any) => {
                    if (err) { return next(err); }
                    return res.json({
                        success: 1,
                        data: results
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }

    /** Function to fetch monthly sales order */
    static async getMonthlySales(req: any, res: any, next: any) {
        try {
            Order.getMonthlySales((err: any, results: any) => {
                    if (err) { return next(err); }
                    return res.json({
                        success: 1,
                        data: results
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }
}