require("dotenv").config();
import Menu from "../models/Menu";

export default class MenuController {
    
    /** Function to fetch all menus */
    static async getMenus(req: any, res: any, next: any) {
        const offset = req.query.offset;
        try {
            Menu.getMenus(offset, (err: any, results: any) => {
                    if (err) { return next(err); }
                    return res.json({
                        success: 1,
                        data: results
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }

    /** Function to fetch a menu */
    static async getMenu(req: any, res: any, next: any) {
        const id = req.params.id;
        try {
            Menu.getMenu(id, (err: any, results: any) => {
                    if (err) { return next(err); }
                    if (results.length === 0) {
                        return res.json({
                            success: 0,
                            message: "Menu not found"
                        });
                    }
                    return res.json({
                        success: 1,
                        data: results
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }

    /** Function to create Menu */
    static async createMenu(req: any, res: any, next: any) {
        const body = req.body;
        try {
            Menu.createMenu(body, (error: any, results: any) => {
                    if (error) { return next(error); }
                    return res.json({
                        success: 1,
                        data: results
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }

    /** Function to update */
    static async updateMenu(req: any, res: any, next: any) {
        const body = req.body;
        try {
            Menu.updateMenu(body, (error: any, results: any) => {
                    if (error) { return next(error); }
                    if (results.length === 0) {
                        return res.json({
                            success: 0,
                            message: "Failed to update menu"
                        });
                    }
                    return res.json({
                        success: 1,
                        message: "Menu data updated successfully"
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }

    /** Function to delete */
    static async deleteMenu(req: any, res: any, next: any) {
        const id = req.params.id;
        try {
            Menu.deleteMenu(id, (error: any, results: any) => {
                    if (error) { return next(error); }
                    if (results.length === 0) {
                        return res.json({
                            success: 0,
                            message: "Menu not found"
                        });
                    }
                    return res.json({
                        success: 1,
                        message: "Menu removed successfully"
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }
}