require("dotenv").config();
import Category from "../models/Category";

export default class CategoryController {
    
    /** Function to fetch all categories */
    static async getCategories(req: any, res: any, next: any) {
        try {
            Category.getCategories((err: any, results: any) => {
                if (err) { return next(err); }
                    return res.json({
                        success: 1,
                        data: results
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }
}