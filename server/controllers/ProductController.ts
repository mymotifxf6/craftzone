require("dotenv").config();
import Product from "../models/Product";

export default class ProductController {
    
    /** Function to fetch all items */
    static async getProducts(req: any, res: any, next: any) {
        try {
            Product.getProducts((err: any, results: any) => {
                if (err) { return next(err); }
                    return res.json({
                        success: 1,
                        data: results
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }

    /** Function to fetch a product */
    static async getProduct(req: any, res: any, next: any) {
        const id = req.params.id;
        try {
            Product.getProduct(id, (err: any, results: any) => {
                    if (err) { return next(err); }
                    if (results.length === 0) {
                        return res.json({
                            success: 0,
                            message: "Item not found"
                        });
                    }
                    return res.json({
                        success: 1,
                        data: results
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }

    /** Function to fetch a product reviews */
    static async getReviews(req: any, res: any, next: any) {
        const id = req.params.id;
        try {
            Product.getReviews(id, (err: any, results: any) => {
                    if (err) { return next(err); }
                    if (results.length === 0) {
                        return res.json({
                            success: 0,
                            message: "Reviews not found"
                        });
                    }
                    return res.json({
                        success: 1,
                        data: results
                    });
                }
            );
        } catch (error) {
            next(error);
        }
    }
}