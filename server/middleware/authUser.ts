import { verify } from 'jsonwebtoken';

export const authUser = (req: any, res: any, next: any) => {
    const authorization = req.headers.authorization;
    if (authorization) {
        const token = authorization.slice(7, authorization.length); // Bearer XXXXXX
        verify(
            token,
            process.env.TOKEN_KEY || 'somethingsecret',
            (err: any, decode: any) => {
                if (err) {
                    res.status(401).send({ message: 'Invalid Token' });
                } else {
                    req.user = decode;
                    next();
                }
            }
        );
    } else {
        res.status(401).send({ message: 'No Token' });
    }   
};

export default authUser;