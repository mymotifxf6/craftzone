import { Router } from 'express';
import OrderController from '../controllers/OrderController';

class OrderRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.getRoutes();
    }

    getRoutes() {
        this.router.get('/', OrderController.getOrders);
        this.router.get('/monthsales', OrderController.getMonthlySales);
    }
}

export default new OrderRouter().router;