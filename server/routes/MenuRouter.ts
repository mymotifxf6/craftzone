import { Router } from 'express';
import MenuController from '../controllers/MenuController';

class MenuRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.getRoutes();
        this.postRoutes();
        this.patchRoutes();
        this.deleteRoutes();
    }

    getRoutes() {
        this.router.get('/', MenuController.getMenus);
        this.router.get('/:id', MenuController.getMenu);
    }

    postRoutes() {
        this.router.post('/create', MenuController.createMenu);
    }

    patchRoutes() {
        this.router.patch('/update', MenuController.updateMenu);
    }

    deleteRoutes() {
        this.router.delete('/delete/:id', MenuController.deleteMenu);
    }
}

export default new MenuRouter().router;