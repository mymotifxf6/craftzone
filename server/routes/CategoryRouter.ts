import { Router } from 'express';
import CategoryController from '../controllers/CategoryController';
import authUser from '../middleware/authUser';

class CategoryRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.getRoutes();
    }
    
    getRoutes() {
        //this.router.get('/', authUser, CategoryController.getCategories);
        this.router.get('/', CategoryController.getCategories);
    }
}

export default new CategoryRouter().router;