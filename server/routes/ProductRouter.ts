import { Router } from 'express';
import ProductController from '../controllers/ProductController';
import CategoryController from '../controllers/CategoryController';

class ProductRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.getRoutes();
    }

    getRoutes() {
        this.router.get('/', ProductController.getProducts);
        this.router.get('/:id', ProductController.getProduct);
        this.router.get('/reviews/:id', ProductController.getReviews);
        this.router.get('/categories', CategoryController.getCategories);
    }
}

export default new ProductRouter().router;