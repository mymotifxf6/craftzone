import { Router } from 'express';
import ItemController from '../controllers/ItemController';

class MenuRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.getRoutes();
        this.postRoutes();
        this.patchRoutes();
        this.deleteRoutes();
    }

    getRoutes() {
        this.router.get('/', ItemController.getItems);
        this.router.get('/:id', ItemController.getItem);
    }

    postRoutes() {
        this.router.post('/create', ItemController.createItem);
    }

    patchRoutes() {
        this.router.patch('/update', ItemController.updateItem);
    }

    deleteRoutes() {
        // this.router.delete('/delete/:id', ItemController.deleteMenu);
    }
}

export default new MenuRouter().router;