import { Router } from 'express';
import UserController from '../controllers/UserController';

class UserRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        // this.getRoutes();
        this.postRoutes();
        // this.patchRoutes();
        // this.deleteRoutes();
    }

    // getRoutes() {
    //     this.router.get('/', MenuController.getMenus);
    //     this.router.get('/:id', MenuController.getMenu);
    // }

    postRoutes() {
        this.router.post('/signin', UserController.getUser);
        this.router.post('/create', UserController.createUser);
    }

    // patchRoutes() {
    //     this.router.patch('/update', MenuController.updateMenu);
    // }

    // deleteRoutes() {
    //     this.router.delete('/delete/:id', MenuController.deleteMenu);
    // }
}

export default new UserRouter().router;