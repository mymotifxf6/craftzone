import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
// Redux Imports

import HomeScreen from './pages/HomeScreen';
import ProductDetails from './pages/ProductDetails';
import LoginScreen from './pages/LoginScreen';
import RegisterScreen from './pages/RegisterScreen';

// import './App.css';

function App() {
  return (
      <div className="App">
        <React.Fragment>
          <BrowserRouter>
            <Switch>
              <Route exact path="/" component={HomeScreen}></Route>
              <Route exact path="/prodetails" component={ProductDetails}></Route>
              <Route exact path="/signin" component={LoginScreen}></Route>
              <Route exact path="/register" component={RegisterScreen}></Route>
            </Switch>
          </BrowserRouter>
        </React.Fragment>
      </div>
  );
}

export default App;
