export enum CategoryActionTypes {
    FETCH_ALL_CATEGORIES = "Fetch All Categories",
    LOAD_ORDERS_START = "Load Orders Start",
    LOAD_ORDERS_STOP = "Load Orders Stop"
}

export class CategoryActions {
    static startLoadOrderAction = () => ({
        type: CategoryActionTypes.LOAD_ORDERS_START
    })

    static stopLoadOrderAction = () => ({
        type: CategoryActionTypes.LOAD_ORDERS_STOP
    })

    static fetchCategoryActions = (data:any[]) => ({
        type: CategoryActionTypes.FETCH_ALL_CATEGORIES,
        payload: data,
    })
}