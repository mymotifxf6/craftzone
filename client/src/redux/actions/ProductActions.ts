export enum ProductActionTypes {
    START_FECTHING_PRODUCTS = "Start Fetching All Products",
    STOP_FECTHING_PRODUCTS = "Stop Fetching All Products",
    FETCH_ALL_PRODUCTS = "Fetch All Products",
    FETCH_PRODUCT_REVIEW = "Fetch Product Review",
    FETCH_PRODUCT = "Fetch Product",
    SHOW_TOAST_MSG = "Show Toast Msg",
    HIDE_TOAST_MSG = "Hide Toast Msg",
}


export class ProductActions {
    static startFetchingProductAction = () => ({
        type:ProductActionTypes.START_FECTHING_PRODUCTS,
    });

    static stopFetchingProductAction = () => ({
        type:ProductActionTypes.STOP_FECTHING_PRODUCTS,
    });

    static fetchAllProductAction = (data:any[]) => ({
        type: ProductActionTypes.FETCH_ALL_PRODUCTS,
        payload: data,
    });

    static fetchSingleProductAction = (data:any[]) => ({
        type: ProductActionTypes.FETCH_PRODUCT,
        payload: data,
    });

    static fetchProductReviewAction = (data: any[]) => ({
        type: ProductActionTypes.FETCH_PRODUCT_REVIEW,
        payload: data,
    })

    static showToast = (data:any) => ({
        type: ProductActionTypes.SHOW_TOAST_MSG,
        payload: data,
    });   

    static hideToast = () => ({
        type: ProductActionTypes.HIDE_TOAST_MSG,
    });
}