export enum OrderActionTypes {
    FETCH_ALL_ORDERS = "Fetch All Orders",
    FETCH_MONTHLY_SALES = "Fetch Monthly Sales",
}

export class OrderActions {
    static fetchOrderAction = (data:any[]) => ({
        type: OrderActionTypes.FETCH_ALL_ORDERS,
        payload: data,
    });
    static fetchMonthlySalesAction = (data:any[]) => ({
        type: OrderActionTypes.FETCH_MONTHLY_SALES,
        payload: data,
    });

}