export enum UserActionTypes {
    USER_REGISTER_START = "User Register Start",
    USER_REGISTER_END = "User Register End",
    USER_SIGNIN_REQUEST = "User Signin Request",
    USER_SIGNIN_SUCCESS = "User Signin Success",
    USER_SIGNIN_FAIL = "User Signin Fail",
    USER_SIGNIN_FAIL_CLEAR = "User Signin Fail Clear",
    USER_SIGNOUT_REQUEST = "User Signout Request",
    USER_SIGNOUT_SUCCESS = "User Signout Success",
    HIDE_MODAL_MSG = "Hide Modal Msg",
}

export class UserActions {
    static userRegisterStartsAction = () => ({
        type: UserActionTypes.USER_REGISTER_START,        
    });
    
    static userRegisterEndAction = (data: any) => ({
        type: UserActionTypes.USER_REGISTER_END,
        payload: data,
    });

    static userSigninRequest = () => ({
        type: UserActionTypes.USER_SIGNIN_REQUEST,
    });

    static userSigninSuccess = (data: any) => ({
        type: UserActionTypes.USER_SIGNIN_SUCCESS,
        payload: data,
    });

    static userSigninFail = (data: any) => ({
        type: UserActionTypes.USER_SIGNIN_FAIL,
        payload: data,
    });

    static userSigninFailClear = () => ({
        type: UserActionTypes.USER_SIGNIN_FAIL_CLEAR
    });

    static userSignoutRequest = () => ({
        type: UserActionTypes.USER_SIGNOUT_REQUEST
    });

    static userSignoutSuccess = () => ({
        type: UserActionTypes.USER_SIGNOUT_SUCCESS
    });

    static hideModal = () => ({
        type: UserActionTypes.HIDE_MODAL_MSG
    });
}