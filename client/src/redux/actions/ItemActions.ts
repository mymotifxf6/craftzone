export enum ItemActionTypes {
    FETCH_ALL_ITEMS = "Fetch All Items",
    SHOW_TOAST_MSG = "Show Toast Msg",
    HIDE_TOAST_MSG = "Hide Toast Msg",
}


export class ItemActions {
    static fetchItemAction = (data:any[]) => ({
        type: ItemActionTypes.FETCH_ALL_ITEMS,
        payload: data,
    })

    static showToast = (data:any) => ({
        type: ItemActionTypes.SHOW_TOAST_MSG,
        payload: data,
    })    

    static hideToast = () => ({
        type: ItemActionTypes.HIDE_TOAST_MSG,
    })
}