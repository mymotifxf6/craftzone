import { OrderActionTypes } from '../actions/OrderActions';

interface OrderReducerState {
    orders: any;
    monthlySales: any;
}

const initialState: OrderReducerState = {
    orders: null,
    monthlySales: null,
}

export const OrderReducer = (state=initialState, action: any): OrderReducerState => {
    switch(action.type) {
        case OrderActionTypes.FETCH_ALL_ORDERS: {
            return { ...state, orders: action.payload }
        }
        case OrderActionTypes.FETCH_MONTHLY_SALES: {
            return { ...state, monthlySales: action.payload }
        }
        default: {
            return state;
        }
    }
}