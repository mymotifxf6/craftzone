import {applyMiddleware, combineReducers, createStore, compose} from 'redux';
import thunk from 'redux-thunk';
import { ItemReducer } from './ItemReducer';
import { CategoryReducer } from './CategoryReducer';
import { OrderReducer } from './OrderReducer';
import { ProductReducer } from './ProductReducer';
import { UserReducer } from './UserReducer';

export const rootReducer = combineReducers({
    itemReducer: ItemReducer,
    categoryReducer: CategoryReducer,
    orderReducer: OrderReducer,
    productReducer: ProductReducer,
    userReducer: UserReducer,
});

const composeEnhancers = compose;

export default createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));