import { ProductActionTypes } from '../actions/ProductActions';

interface ProductReducerState {
    products: any[];
    product: any;
    reviews: any[];
    toastFlag: number;
    loadingFlag: number;
    openSnack: boolean;
}

const initialState: ProductReducerState = {
    products: [],
    reviews: [],
    product: null,
    toastFlag: 0,
    loadingFlag: 0,
    openSnack: false,
}

export const ProductReducer = (state = initialState, action: any): ProductReducerState => {
    switch(action.type) {
        case ProductActionTypes.START_FECTHING_PRODUCTS: {
            return { ...state, loadingFlag: 1 }
        }
        case ProductActionTypes.STOP_FECTHING_PRODUCTS: {
            return { ...state, loadingFlag: 0 }
        }
        case ProductActionTypes.FETCH_ALL_PRODUCTS: {
            return { ...state, products: action.payload }
        }
        case ProductActionTypes.FETCH_PRODUCT: {
            return { ...state, product: action.payload }
        }
        case ProductActionTypes.SHOW_TOAST_MSG: {
            return { ...state, toastFlag: action.payload, openSnack: true }
        }
        case ProductActionTypes.HIDE_TOAST_MSG: {
            return { ...state, openSnack: false }
        }
        case ProductActionTypes.FETCH_PRODUCT_REVIEW: {
            return { ...state, reviews: action.payload }
        }
        default: {
            return state;
        }
    }
}