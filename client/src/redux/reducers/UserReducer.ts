import { UserActionTypes } from '../actions/UserActions';

interface UserReducerState {
    userInfo: any;
    message: string;
    loadingFlag: number;
    modalMsg: boolean;
}

const initialState: UserReducerState = {
    userInfo: null,
    message: '',
    loadingFlag: 0,
    modalMsg: false,
}

export const UserReducer = (state=initialState, action: any): UserReducerState => {
    switch(action.type) {        
        case UserActionTypes.USER_REGISTER_START: {
            return { ...state, loadingFlag: 1 }
        }

        case UserActionTypes.USER_REGISTER_END: {
            return { ...state, loadingFlag: 0, message: action.payload }
        }

        case UserActionTypes.USER_SIGNIN_REQUEST: {
            return { ...state, loadingFlag: 1 }
        }

        case UserActionTypes.USER_SIGNIN_SUCCESS: {
            return { ...state, loadingFlag: 0, modalMsg: true, userInfo: action.payload }
        }        

        case UserActionTypes.USER_SIGNIN_FAIL: {
            return { ...state, modalMsg: true, userInfo: action.payload }
        }

        case UserActionTypes.USER_SIGNIN_FAIL_CLEAR: {
            return { ...state, modalMsg: false, userInfo: null }
        }

        case UserActionTypes.USER_SIGNOUT_REQUEST: {
            localStorage.removeItem('userInfo');
            return { ...state, loadingFlag: 1, modalMsg: true, userInfo: null }
        }

        case UserActionTypes.USER_SIGNOUT_SUCCESS: {
            document.location.href = '/';
            return { ...state, loadingFlag: 0, modalMsg: false }
        }

        case UserActionTypes.HIDE_MODAL_MSG: {
            return { ...state, modalMsg: false }
        }
        
        default: {
            return state;
        }
    }
}