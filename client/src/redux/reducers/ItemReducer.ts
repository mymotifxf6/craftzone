import { ItemActionTypes } from '../actions/ItemActions';

interface ItemReducerState {
    items: any[];
    toastFlag: number;
    openSnack: boolean;
}

const initialState: ItemReducerState = {
    items: [],
    toastFlag: 0,
    openSnack: false,
}

export const ItemReducer = (state = initialState, action: any): ItemReducerState => {
    switch(action.type) {
        case ItemActionTypes.FETCH_ALL_ITEMS: {
            return { ...state, items: action.payload }
        }
        case ItemActionTypes.SHOW_TOAST_MSG: {
            return { ...state, toastFlag: action.payload, openSnack: true }
        }
        case ItemActionTypes.HIDE_TOAST_MSG: {
            return { ...state, openSnack: false }
        }
        default: {
            return state;
        }
    }
}