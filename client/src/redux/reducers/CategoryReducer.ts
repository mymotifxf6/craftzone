import { CategoryActionTypes } from '../actions/CategoryActions';

interface CategoryReducerState {
    productCategoryList: any[];
    loadingFlag: number;
}

const initialState: CategoryReducerState = {
    productCategoryList: [],
    loadingFlag: 0,
}

export const CategoryReducer = (state=initialState, action: any): CategoryReducerState => {
    switch(action.type) {
        case CategoryActionTypes.LOAD_ORDERS_START: {
            return { ...state, loadingFlag: 1};
        }
        case CategoryActionTypes.LOAD_ORDERS_STOP: {
            return { ...state, loadingFlag: 0};
        }
        case CategoryActionTypes.FETCH_ALL_CATEGORIES: {
            // return { ...state, productCategoryList: [...state.productCategoryList, ...action.payload]};
            return { ...state, productCategoryList: action.payload };
        }
        default: {
            return state;
        }
    }
}