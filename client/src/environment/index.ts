import {DevEnvironment} from './dev.env';

export interface Environment {
    base_url: string,
}

export function getEnvVariables() : Environment {
    return DevEnvironment;
}