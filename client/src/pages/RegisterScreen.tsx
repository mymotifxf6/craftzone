import React, { useState, useEffect, useReducer } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link as Hyperlink, useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { ArrowBack } from '@material-ui/icons';
import { IconButton, Toolbar, MenuItem, Link, Typography, Alert, AlertTitle, Stack, Modal, Fade, Backdrop, Box, Container, CssBaseline } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { UserApiRepository } from "../services/users/UserApiRepository";
import Forms from '../components/Forms';
import AppBar from '../components/AppBar';
import ValidateInfo from '../ValidateInfo';

const useStyles = makeStyles((theme) => ({   
	menuButton: {
		marginRight: theme.spacing(2),
	},
	title: {
		flexGrow: 1,
	},
	modalBox: {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		width: '50%',
		bgcolor: 'background.paper',
		border: '2px solid #000',
		boxShadow: '24',
		p: '4'
	}
}));

/** Assigning the initial values for the form fields */
const initialState = {
	name: '',
	email: '',
	mobile: '',
	password: '',
	confirmpassword: '',
};

const formReducer = (state: any, action: any) => {
    switch (action.type) {
        case 'UPDATE_INPUT': return { ...state, [action.field]: action.payload, };
		case 'RESET_FORM':
			return {
				...initialState
			}
        default: return state;
    }
};

const theme = createTheme();

export default function RegisterScreen(props: any) {
	const classes = useStyles();	
	const actionDispatch = useDispatch();
	const history = useHistory();
	const [{ 
		name,
		email,
		mobile,
		password,
		confirmpassword,
	}, setState] = useState(initialState);

	const clearState = () => { setState({ ...initialState }); };
	const [userForm, dispatch] = useReducer(formReducer, initialState);
	const loading: number = useSelector((state: any) => (state.userReducer.loadingFlag));
	const message: string = useSelector((state: any) => (state.userReducer.message));

	const [open, setOpen] = useState(false);

  	// const handleOpen = () => setOpen(true);
  	const handleClose = () => setOpen(false);
	
	const inputHandler = (e: any) => {
		e.preventDefault();
		dispatch({
			type: "UPDATE_INPUT",
			field: e.target.name,
			payload: e.target.value
		});
	}

	const createUsers = async(data: any) => {
		await actionDispatch(UserApiRepository.createUsers(data));
		dispatch({ type: 'RESET_FORM' });
	}

	const submitButton = () => {
        if (userForm.password !== userForm.confirmpassword) {
			alert('Password & Confirm password does not match');
		} else {
			createUsers(userForm);
			// console.log(userForm);
			const errors = ValidateInfo(userForm);
			if (Object.keys(errors).length > 0) {
				
			} else {
				//createUsers(userForm);
			}
		}
    }

	useEffect(() => {		
		if (message !== '') { 
			setOpen(true);
			/* Close alert modal box after 5 seconds */
			setTimeout(function () {
				setTimeout(function () {
					setOpen(false);
				}, 4000);
				props.history.push('/');
			}, 4000);

			return() => {
				clearState();
			}
		}}, [message]);

	return (
		<ThemeProvider theme={ theme }>
			<AppBar position="static" menu={false}>
				<Toolbar>
					<IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={()=>history.goBack()}>
						<ArrowBack />
					</IconButton>
					<Typography variant="inherit"></Typography>
					<Typography variant="h6" className={classes.title}>
					</Typography>					
					<MenuItem>
						<Link color="inherit" onClick={() => { alert("You clicked on Cart"); }}>Cart</Link>
					</MenuItem>
					<MenuItem>
						<Link color="inherit" onClick={() => { alert("You clicked on Sign In"); }}>Sign In</Link>
					</MenuItem>
				</Toolbar>
			</AppBar>
            <Container component="main" maxWidth="xs">				
				<CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex', 
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
					<Typography component="h1" variant="h5">
                        Sign up
                    </Typography>
					<Box component="form" noValidate sx={{ mt: 1 }}>
						<Forms 
							title="User Registration"
							userForm={userForm}
							fields={["name", "email", "mobile", "password", "confirmpassword", "button"]}
							buttonTitle={"Register"}
							inputHandler={(e: any) => inputHandler(e)}
							submitButton={submitButton}
						/>
					</Box>
				</Box>

				<Modal
					aria-labelledby="transition-modal-title"
					aria-describedby="transition-modal-description"
					open={ open ? true : false }
					onClose={handleClose}
					closeAfterTransition
					BackdropComponent={Backdrop}
					BackdropProps={{
						timeout: 500,
					}}
				>
					<Fade in={open}>
						<Box className={classes.modalBox}>
							<Stack sx={{ width: '100%' }} spacing={2}>
								<Alert severity="success">
									<AlertTitle>Success</AlertTitle>
									{ message !== '' ? `${message} your registration is successful` : 'This is a success alert — <strong>check it out!</strong>' }
								</Alert>
							</Stack>
						</Box>
					</Fade>
				</Modal>
			</Container>
		</ThemeProvider>
	)
}