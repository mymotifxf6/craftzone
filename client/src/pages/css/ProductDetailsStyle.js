import { makeStyles } from '@material-ui/core/styles';

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },   
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    rowTop: {
        alignItems: "flex-start"
    },
    col1: {
        flex: "1 1 25rem"
    },
    col2: {
        flex: "2 1 50rem",        
    },
    main: {
        gridArea: "header",
        padding: "1em"
    },
    card: {
        border: "0.1rem #c0c0c0 solid",
        backgroundColor: "#f8f8f8",
        borderRadius: "0.5rem",
        margin: "1rem"
    },
    cardBody: {
        padding: "1rem",
        "$>*": {
            marginBottom: "0.5rem"
        }
    },
    row: {
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-between",
        alignItems: "center"
    }    
}));

const imageStyle = {
	width: '100%',
    height: '100%',
    position: 'relative',
}
const clearPadding = {
    paddingTop : 0
}

export { useStyles, imageStyle, clearPadding };