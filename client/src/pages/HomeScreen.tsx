import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import clsx from 'clsx';
import { makeStyles, CssBaseline, Container, Grid, Paper, CircularProgress } from '@material-ui/core';
import { Link } from 'react-router-dom';
import AppBar from '../components/AppBar';
import SideDrawer from '../components/SideDrawer';
import Product from '../components/Product';
import FlashMessage from '../components/FlashMessage';
import { ProductApiRepository } from '../services/products/ProductApiRepository';
import { CategoryApiRepository } from '../services/categories/CategoryApiRepository';
import { UserApiRepository } from '../services/users/UserApiRepository';
import { title } from 'process';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },   
    fixedHeight: {
        height: 'auto',
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    loaderBox: {
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
    },
    loader: {
        position: 'absolute',
        top: '50%',
        right: '50%',       
    }

}));

export default function HomeScreen() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const loading: number = useSelector((state: any) => (state.productReducer.loadingFlag));
    const products: any[] = useSelector((state: any) => (state.productReducer.products));
    const productCategoryList = useSelector((state: any) => (state.categoryReducer.productCategoryList));
    const userInfo = JSON.parse(localStorage.getItem('userInfo') || '{}');
    const modal = useSelector((state: any) => (state.userReducer.modalMsg));
    const [drawer, setDrawer] = useState(false);

    const [term, setTerm] = useState("");

    useEffect(() => {
        // console.log(React.version);
        fetchProducts();
        fetchCategories();
	}, [dispatch]);
    
    const fetchProducts = async() => {
		await dispatch(ProductApiRepository.fetchProducts());        
	}

    const fetchCategories = async() => {
        await dispatch(CategoryApiRepository.fetchCategories());
    }

    const handleDrawerOpen = () => {
        setDrawer(true);
    };

    const handleDrawerClose = () => {
        setDrawer(false);
    };

    const handleClose = () => {        
        dispatch(UserApiRepository.hideModal());
        // setModal(!modal);
    }

    /** Open flash message on any event triggered */
    const flashMsg = () => {
        if (userInfo === null && modal) {
            return ( 
                <FlashMessage 
                    open={ modal }
                    type="info"
                    message={ 'Logged Out Successfully' }
                    onClose={setTimeout(() => { handleClose() }, 3000)}
			    />
            );
        }
    }

    /** User signout function */
    const logoutHandler = async() => {        
        await dispatch(UserApiRepository.signOut());
    }

    /** Set the term searched in the term state */
    const searchTermHandler = (e: string) => {        
        setTerm(e);
    }

    /** Filter the term as soon as the term state is updated */
    let filteredProducts = products.filter((d: any) => {
        const { title } = d;
        return title.toLowerCase().indexOf(term.toLowerCase()) > -1;
    })
    .map((product: any, index: any) => {
        return <Product key={product.id} product={product}></Product>
    })
    
    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar
                menu={true}
                openDrawer = { drawer }
                handleDrawerOpen = {() => handleDrawerOpen()}
                searchTermHandler = {(e: string) => searchTermHandler(e)}
            />
            <SideDrawer 
                closeDrawer = { drawer }
                handleDrawerClose = {() => handleDrawerClose()}
                usrData = { userInfo }
                category = { productCategoryList }
                logoutHandler={ () => logoutHandler() }
            />
            <main className={classes.content}>
                <div className={classes.appBarSpacer} />
                <Container maxWidth="lg" className={classes.container}>
                    <Grid container spacing={4}>                       
                        { filteredProducts }
                    </Grid>
                </Container>
            </main>
        </div>
    )
}