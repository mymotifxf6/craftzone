import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Rating from '../components/Rating';
// import { makeStyles } from '@material-ui/core/styles';
import { useStyles, clearPadding, imageStyle } from './css/ProductDetailsStyle';
import AppBar from '../components/AppBar';
import Buttons from '../components/Buttons';
import Image from 'material-ui-image';
import { ProductApiRepository } from '../services/products/ProductApiRepository';
import moment from 'moment';
import { Typography, Box, NativeSelect, FormControl, Toolbar, IconButton, MenuItem, Link, Grid, List, ListItem, ListItemText } from '@mui/material';
import { ArrowBack } from '@material-ui/icons';
import { useHistory } from 'react-router-dom';

export default function ProductDetails(props: any) {
	const classes = useStyles();
	const dispatch = useDispatch();
	const history = useHistory();

	const [qty, setQty] = useState(1);
	const product: any[] = useSelector((state: any) => (state.productReducer.product));
	const reviews: any[] = useSelector((state: any) => (state.productReducer.reviews));
	const userInfo = JSON.parse(localStorage.getItem('userInfo') || '{}');

	useEffect(() => {		
		window.scrollTo(0, 0);
		if (props.location.state.id) {
			fetchProduct(props.location.state.id);
			fetchReviews(props.location.state.id);
		}
	}, []);

	const fetchProduct = async (id: number) => {
		await dispatch(ProductApiRepository.fetchProduct(id));
	}

	// Get the product reviews
	const fetchReviews = async (id: number) => {
		await dispatch(ProductApiRepository.fetchReviews(id));
	}

	/** Check if userInfo is null or not for the logged in user */
    const checkUser = () => {
        if (userInfo !== null && userInfo.success > 0 && Object.keys(userInfo).length > 0) {
            return ( <AppBar title="Logout" path="signout" /> );
        } else {
            return ( <AppBar title="Sign In" path="signin" /> );
        }
    }

	if (product !== null && product.length > 0) {
		console.log(product);
		return (
			<div className={classes.root}>
            	<AppBar position="static" menu={false}>
					<Toolbar>
						<IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={()=>history.goBack()}>
							<ArrowBack />
						</IconButton>
						<Typography variant="inherit"></Typography>
						<Typography variant="h6" className={classes.title}>
						</Typography>					
						<MenuItem>
							<Link color="inherit" onClick={() => { alert("You clicked on Cart"); }}>Cart</Link>
						</MenuItem>
						<MenuItem>
							<Link color="inherit" onClick={() => { alert("You clicked on Sign In"); }}>Sign In</Link>
						</MenuItem>
					</Toolbar>
				</AppBar>
				<div className={classes.main}>
					<Grid container spacing={8}
						direction="row"
						justifyContent="center"
						alignItems="flex-start"
					>
						<Grid item xs={6}>
							<div className="col-2">
								<Image
									style={clearPadding}
									imageStyle={ imageStyle }
									src={ product[0].image }
								/>
							</div>
							<div className="col-1">
								<ul style={{ padding: 0, margin: 0, listStyleType: "none" }}>
									<li style={{ marginTop: "1em" }}>
										<h4>
											<Rating rating={product[0].ratings} caption=" Average Review" numReviews={product[0].ratings} />
										</h4>
									</li>
									<li style={{ marginTop: "1em" }}>
										{/* <Rating rating={product[0].ratings} numReviews={product[0].ratings} totalReviews={reviews.length}></Rating> */}
										<Typography variant="h5" className={classes.title}>{reviews.length} reviews</Typography>
									</li>
									<li style={{ marginTop: "1em" }}><Typography variant="h5" className={classes.title}>Comments</Typography></li>									
									<li style={{ marginTop: "1em" }}>
										<div>
											<ul style={{ padding: 0, margin: 0, listStyleType: "none" }}>
											{reviews.length > 0 && reviews.map((review: any, index: any) => (
												<li key={index} style={{ marginTop: "1em" }}>
													<strong>{review.name}</strong>
													<p>
														<Rating rating={review.rating} caption={moment(review.createdAt).format('MMMM Do YYYY')}></Rating>
													</p>														
													<p>{review.message}</p>
												</li>
											))}
											</ul>
											{reviews.length === 0 && (
												<ul style={{ padding: 0, margin: 0, listStyleType: "none" }}>
													<i>No reviews for this item</i>
												</ul>
											)}
										</div>
									</li>
								</ul>
							</div>
						</Grid>
						<Grid item xs={6}>
							<ul style={{ padding: 0, margin: 0, listStyleType: "none" }}>
								<li style={{ marginTop: "1em" }}><h1>{product[0].title}</h1></li>
								<div className={classes.row}>
									<li style={{ marginTop: "1em" }}>Price: ${product[0].price}</li>
								</div>
								<div className={classes.row}>
									<li style={{ marginTop: "1em" }}>Description:<p>{product[0].description}</p></li>
								</div>
								<div>
									<Buttons 
										proid={ product[0].id }										
										buy={true}
										view={false}
										cancel={false}
									/>
								</div>
							</ul>
						</Grid>
					</Grid>
				</div>
			</div>
		)
	} else {
		return (<div> Product Not Found </div>)
	}
}