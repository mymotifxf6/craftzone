import React, { useEffect, useState, useReducer } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Typography, CssBaseline, Avatar, Box, IconButton, Toolbar, MenuItem, Link } from '@mui/material';
import { LockOutlined } from '@material-ui/icons';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { makeStyles } from '@material-ui/core/styles';
import { Link as Hyperlink, useHistory } from 'react-router-dom';
import Forms from '../components/Forms';
import { validEmail } from '../components/FormValidations';
import { ArrowBack } from '@material-ui/icons';
import { UserApiRepository } from '../services/users/UserApiRepository';
import FlashMessage from '../components/FlashMessage';
import AppBar from '../components/AppBar';

const initialState = {
    email: '',
    password: '',
};

const formReducer = (state: any, action: any) => {
    switch (action.type) {
        case 'UPDATE_INPUT': return { ...state, [action.field]: action.payload, };
        case 'RESET_FORM':
            return {
                ...initialState
            }
        default: return state;
    }
};

const useStyles = makeStyles((theme) => ({   
	menuButton: {
		marginRight: theme.spacing(2),
	},
	title: {
		flexGrow: 1,
	},
}));

const theme = createTheme();

export default function LoginScreen(props: any) {
    const classes = useStyles();
    const history = useHistory();
    const actionDispatch = useDispatch();
    const userInfo = useSelector((state: any) => (state.userReducer.userInfo));
    const [modal, setModal] = useState(true);
    const [userForm, dispatch] = useReducer(formReducer, initialState);
    const errors: string[] = [];

    useEffect(() => {
        if (userInfo !== null && userInfo.success > 0 && Object.keys(userInfo).length > 0) {
            localStorage.setItem('userInfo', JSON.stringify(userInfo));
            props.history.push('/');
        } 
    }, [userInfo]);

    const handleClose = () => {
        actionDispatch(UserApiRepository.userSigninFailClear());
        setModal(false);
    }

    const inputHandler = (e: any) => {
        e.preventDefault();
        dispatch({
            type: "UPDATE_INPUT",
            field: e.target.name,
            payload: e.target.value
        });
    }

    const submitButton = () => {
        if (userForm.email !== '' && validEmail(userForm.email)) { } else {
            errors.push('Please check email');
        }

        if (userForm.password === '') { errors.push('Enter your password'); }

        console.log(errors);
        if (errors.length == 0) {
            userLogin(userForm);
        }
    }

    const userLogin = async(data: any) => {
        data = await actionDispatch(UserApiRepository.signIn(data));
    }

    return (
        <ThemeProvider theme={ theme }>
            <AppBar position="static" menu={false}>
				<Toolbar>
					<IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={()=>history.goBack()}>
						<ArrowBack />
					</IconButton>
					<Typography variant="inherit"></Typography>
					<Typography variant="h6" className={classes.title}>
					</Typography>					
					<MenuItem>
						<Link color="inherit" onClick={() => { alert("You clicked on Cart"); }}>Cart</Link>
					</MenuItem>
					<MenuItem>
						<Link color="inherit" onClick={() => { alert("You clicked on Sign In"); }}>Sign In</Link>
					</MenuItem>
				</Toolbar>
			</AppBar>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex', 
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                        <LockOutlined />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <Box component="form" noValidate sx={{ mt: 1 }}>
                        <form className="form">
                            <Forms
                                userForm={ userForm }
                                fields={["email", "password", "remember", "button"]}
                                buttonTitle={"Sign In"}
                                inputHandler={(e: any) => inputHandler(e)}
                                submitButton={ submitButton }
                                errors={ errors.length > 0 ? errors : null }
                            />
                        </form>
                        <div>
                            <label />
                            <div>
                                New customer?{' '}
                                <Hyperlink to={"register"}>
                                    Create your account
                                </Hyperlink>
                            </div>
                        </div>
                    </Box>
                </Box>

                {userInfo !== null && userInfo.success === 0 && (
                    <FlashMessage 
                        open={ modal }
                        type="error"
                        message={ userInfo.message }
                        onClose={setTimeout(() => { handleClose() }, 3000)}
                    />
			    )}
            </Container>
        </ThemeProvider>
    )
}
