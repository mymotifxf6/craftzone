import CircularProgress from '@mui/material/CircularProgress';

export default function CircularIndeterminate() {
  return (
    <div style={{ alignItems: "center", display: "flex", justifyContent: "center" }}>        
        <span style={{ justifyContent: "center", position: "fixed", top: "55%" }}><CircularProgress />Loading...please wait</span>
    </div>
  );
}