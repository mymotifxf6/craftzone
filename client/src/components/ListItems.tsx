import React, { useEffect } from 'react';
import { ListItem, ListItemIcon, ListItemText, ListSubheader, Divider } from '@material-ui/core';
import { Dashboard, ShoppingCart, People } from '@material-ui/icons';
import { Link } from 'react-router-dom';

export default function Listitems(props: any) {

    useEffect(() => {
        // Unmount when not required
        //console.log("mounted");
        return () => {
            //console.log("cleaned up");
        };
    })

    /** Check if userInfo is null or not for the logged in user */
    const checkUser = () => {        
        if (Object.keys(props.usrData).length > 0) {
            return ( <Link to="#" onClick={ props.logoutHandler } ><ListItemText primary="Logout" /></Link> );
        } else {
            return ( <Link className="brand" to="/signin"><ListItemText primary="Signin" /></Link> );
        }
    }

    return (
        <>
            <div>
                <ListItem button>
                    <ListItemIcon>
                    <Dashboard />
                    </ListItemIcon>
                    <ListItemText primary="Dashboard" />
                </ListItem>
                <ListItem button>
                    <ListItemIcon>
                        <ShoppingCart />
                    </ListItemIcon>
                    <ListItemText primary="Orders" />
                </ListItem>
                <ListItem button>
                    <ListItemIcon>
                        <People />
                    </ListItemIcon>
                    { checkUser() }
                </ListItem>
            </div>
            <Divider />
            {Object.keys(props.category).length > 0 && (
                <div>
                    <ListSubheader inset>Categories</ListSubheader>
                    {props.category.map((val: any, ind: any) => (
                        <ListItem button key={ind}>
                            <Link to="#"><ListItemText primary={val.title} /></Link>
                        </ListItem>
                    ))}
                </div>
            )}            
        </>
    )    
}