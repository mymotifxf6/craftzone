export default function Rating(props) {
    const { rating, numReviews, caption, totalReviews } = props;

    const display = () => {
        let concat = '';
        if (caption) {
            concat += ` ${caption}`;
        }

        if (numReviews !== '' && typeof numReviews !== 'undefined') {
            concat += ` ${numReviews}`;
        }

        if (totalReviews != '' && typeof totalReviews !== 'undefined') {            
            concat += ` ${totalReviews}`;
        }

        if (numReviews === null) {            
            concat += 'No reviews';           
        }
        return concat;  
    }

    return (
        <div className="rating">
        <span>
            <i
            className={
                rating >= 1
                ? 'fa fa-star'
                : rating >= 0.5
                ? 'fa fa-star-half-o'
                : 'fa fa-star-o'
            }
            ></i>
        </span>
        <span>
            <i
            className={
                rating >= 2
                ? 'fa fa-star'
                : rating >= 1.5
                ? 'fa fa-star-half-o'
                : 'fa fa-star-o'
            }
            ></i>
        </span>
        <span>
            <i
            className={
                rating >= 3
                ? 'fa fa-star'
                : rating >= 2.5
                ? 'fa fa-star-half-o'
                : 'fa fa-star-o'
            }
            ></i>
        </span>
        <span>
            <i
            className={
                rating >= 4
                ? 'fa fa-star'
                : rating >= 3.5
                ? 'fa fa-star-half-o'
                : 'fa fa-star-o'
            }
            ></i>
        </span>
        <span>
            <i
            className={
                rating >= 5
                ? 'fa fa-star'
                : rating >= 4.5
                ? 'fa fa-star-half-o'
                : 'fa fa-star-o'
            }
            ></i>
        </span>
        {/* {caption ? (
            <span><i> {caption}</i></span>
        ) : (
                <>
                    {numReviews === null ? (
                        <span>{'No reviews'}</span>
                    ) : (
                        <span>{numReviews + ' reviews'}</span>
                        // <span> {totalReviews + ' reviews'}</span>
                    )}
                    {totalReviews === null ? (
                        <span>{'No reviews'}</span>
                    ) : (
                        <span>{totalReviews + ' reviews'}</span>
                    )}
                </>
        )} */}
        <span>{ display() }</span>
        </div>
    );
}