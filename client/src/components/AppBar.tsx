import React from 'react';
import clsx from 'clsx';
import { makeStyles, AppBar, Toolbar, Typography, IconButton, Badge, Link, MenuItem, InputBase } from '@material-ui/core';
import { styled, alpha } from '@mui/material/styles';
import { Notifications, Menu, Search as SearchIcon, ArrowBack } from '@material-ui/icons';
import { useHistory } from 'react-router-dom'

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    title: {
        flexGrow: 1,
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9),
        },
    },
    appBarSpacer: theme.mixins.toolbar,
        content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
}));

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch',
      },
    },
}));

export default function AppBarr(props:any) {
    const classes = useStyles();
    const history = useHistory();
   
    return (
        <div>
            { props.menu === true && (
                <AppBar position="absolute" className={clsx(classes.appBar, props.openDrawer && classes.appBarShift)}>
                    <Toolbar className={classes.toolbar}>
                        <IconButton
                            edge="start"
                            color="inherit"
                            aria-label="open drawer"
                            onClick={props.handleDrawerOpen}
                            className={clsx(classes.menuButton, props.openDrawer && classes.menuButtonHidden)}
                        >
                            <Menu />
                        </IconButton>
                        <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                            Dashboard
                        </Typography>
                        <Search>
                            <SearchIconWrapper>
                            <SearchIcon />
                            </SearchIconWrapper>
                            <StyledInputBase
                                placeholder="Search…"
                                inputProps={{ 'aria-label': 'search' }}
                                onChange={(e: any) => props.searchTermHandler(e.target.value)}
                            />
                        </Search>
                        <IconButton color="inherit">
                            <Badge badgeContent={4} color="secondary">
                                <Notifications />
                            </Badge>
                        </IconButton>
                    </Toolbar>
                </AppBar>
            )}

            { props.menu === false && (
                <AppBar position="static">
                    <Toolbar>
                        <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={()=>history.goBack()}>
                            <ArrowBack />
                        </IconButton>
                        <Typography variant="inherit"></Typography>
                        <Typography variant="h6" className={classes.title}>
                        </Typography>					
                        <MenuItem>
                            <Link color="inherit" onClick={() => { alert("You clicked on Cart"); }}>Cart</Link>
                        </MenuItem>
                        <MenuItem>
                            <Link color="inherit" onClick={() => { alert("You clicked on Sign In"); }}>Sign In</Link>
                        </MenuItem>
                    </Toolbar>
                </AppBar>
            )}
            
        </div>        
    )
}