import React, { useEffect, useState } from 'react';
import { Alert, AlertTitle, Stack, Modal, Fade, Backdrop, Box } from '@mui/material';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
	menuButton: {
		marginRight: theme.spacing(2),
	},
	title: {
		flexGrow: 1,
	},
	modalBox: {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		width: '50%',
		bgcolor: 'background.paper',
		border: '2px solid #000',
		boxShadow: '24',
		p: '4'
	}
}));

const chkAlertType = (type: string) => {
    if (type === "error") {
        return 'Error';
    } else {
        return 'Success';
    }
}

const FlashMessage = React.memo((props: any) => {
    const classes = useStyles();
    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={ props.open ? true : false }
        >
            <Fade in={ props.open }>
                <Box className={classes.modalBox}>
                    <Stack sx={{ width: '100%' }} spacing={2}>
                        <Alert severity={ props.type }>
                            <AlertTitle>{chkAlertType(props.type)}</AlertTitle>
                            { props.message }
                        </Alert>
                    </Stack>
                </Box>
            </Fade>
        </Modal>
    );
});

export default FlashMessage;