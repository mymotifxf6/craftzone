import React, { useEffect } from 'react';
import { TextField, Button, FormControlLabel, Checkbox } from '@mui/material';

const Forms = (props: any) => {
    const { inputHandler, submitButton, userForm, fields, buttonTitle } = props;

    const display = () => {
        let component: any = [];
        fields.map((val: string, key: number) => {
            switch(val) {
                case 'name': { 
                    component.push(
                        <TextField 
                            key={key}
                            margin="normal"
                            required
                            fullWidth
                            id="name"
                            label="Fullname"
                            value={userForm.name}
                            name="name"
                            autoComplete="name"
                            onChange={inputHandler}
                            autoFocus
                        />
                    );
                }
                break;
                case 'email': {
                    component.push(                        
                        <TextField 
                            key={key}
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            value={userForm.email}
                            name="email"
                            autoComplete="email"
                            onChange={inputHandler}
                            autoFocus
                        />
                    );
                }
                break;
                case 'mobile': { 
                    component.push(
                        <TextField 
                            key={key}
                            margin="normal"
                            required
                            fullWidth
                            id="mobile"
                            label="Mobile Number"
                            value={userForm.mobile}
                            name="mobile"
                            autoComplete="mobile"
                            onChange={inputHandler}
                        />
                   );
                }
                break;
                case 'password': { 
                    component.push(
                        <TextField 
                            key={key}
                            margin="normal"
                            required
                            fullWidth
                            id="password"
                            label="Password"
                            type="password"
                            value={userForm.password}
                            name="password"
                            autoComplete="password"
                            onChange={inputHandler}
                        />
                    );
                }
                break;                
                case 'confirmpassword': { 
                    component.push(
                        <TextField 
                            key={key}
                            margin="normal"
                            required
                            fullWidth
                            id="confirmpassword"
                            label="Confirm Password"
                            value={userForm.confirmpassword}
                            name="confirmpassword"
                            autoComplete="Confirm Password"
                            onChange={inputHandler}
                        />
                    );
                }
                break;
                case 'remember': {
                    component.push(
                        <FormControlLabel
                            key={key}
                            control={<Checkbox value="remember" color="primary" />}
                            label="Remember me"
                        />
                    );
                }
                break;
                case 'button': {
                    component.push(
                        <Button
                            key={key}
                            type="button"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                            onClick={submitButton}
                        >
                            {buttonTitle}
                        </Button>
                    );
                }
            }
        });  
        return component;
    }

    return (
        display()
    )
}

export default Forms;