import React, { Component } from "react";
import { Button } from '@material-ui/core';

const Buttons = (props: any) => {
    return (
        <React.Fragment>
            {props.buy && (
                <Button size="small" variant="contained" color="primary" className="primary block">
                    Add to Cart
                </Button>
            )}
            {props.view && (
                <Button size="small" variant="contained" color="primary" onClick={ props.productViewHandler }>
                    View
                </Button>
            )}
            {props.cancel && (
                <Button size="small" variant="contained" color="primary">
                    Cancel
                </Button>
            )}
        </React.Fragment>
    )
}

export default Buttons;