import { useHistory } from 'react-router-dom';
import { Button, Card, CardActions, CardContent, CardMedia, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Buttons from '../components/Buttons';

const useStyles = makeStyles((theme) => ({
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
        height: 140,
        component: "img"
    },
    cardContent: {
        flexGrow: 1,
    },
    typoDescription: {
        whiteSpace: "nowrap",
        overflow: 'hidden',
        textOverflow: 'ellipsis'
    },
    cardActions: {
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
    }
}));

const ProductItem = (props: any) => {
    const classes = useStyles();
    
    const history = useHistory();
    const productViewHandler = (sku: any) => {
        history.push({
            pathname: '/prodetails',
            state: { sku }
        });
    } 
    
    return (
        <Grid item key={props} xs={12} sm={4} md={4}>
            <Card className={classes.card}>
                <CardMedia
                    className={classes.cardMedia}
                    // image="https://source.unsplash.com/random"
                    image={ props.data.image }
                    title="Image title"
                />
                <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                        { props.data.title }
                    </Typography>
                    <Typography className={classes.typoDescription}>
                        { props.data.description }
                    </Typography>
                </CardContent>
                <CardActions className={classes.cardActions}>                   
                    <Buttons 
                        proid={ props.data.sku }
                        productViewHandler={() => productViewHandler(props.data.sku)}
                        buy={true}
                        view={true}                        
                        cancel={false}
                    />
                </CardActions>
            </Card>
        </Grid>
    )
}

export default ProductItem;