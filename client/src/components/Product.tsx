import { Card, CardActions, CardContent, CardMedia, Grid, Typography } from '@mui/material';
import { makeStyles } from '@material-ui/core/styles';
import Rating from './Rating';
import { Link } from 'react-router-dom';
import Image from 'material-ui-image';

const useStyles = makeStyles((theme) => ({
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
        height: "auto",
        component: "img"
    },    
    cardContent: {
        flexGrow: 1,
    },
    typoTitle: {
        whiteSpace: "nowrap",
        overflow: 'hidden',
        textOverflow: 'ellipsis'
    },
    cardActions: {
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
    }
}));

const imageStyle = {
    height: '100%',
    position: 'relative',
}
const clearPadding = {
    paddingTop : 0
}

export default function Product(props: any) {
    const classes = useStyles();
    const { product } = props;
    return (
        <Grid item key={props} xs={12} sm={4} md={4}>
            <Card className={classes.card}>                
                <Image
                    style={clearPadding}
                    imageStyle={ imageStyle }
                    src={ product.image }
                />
                <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2" className={classes.typoTitle}>
                        <Link to={{ pathname:"prodetails", state: { id:`${product.id}` }}}>
                            { product.title }
                        </Link>
                    </Typography>
                    <Rating
                        rating={product.ratings}
                        numReviews={product.reviews}
                    ></Rating>
                    <Typography variant="h5">
                        {`\u20B9`}{ product.price }
                    </Typography>
                </CardContent>
                <CardActions className={classes.cardActions}>                   
                    {/* <Buttons 
                        proid={ props.data.sku }
                        productViewHandler={() => productViewHandler(props.data.sku)}
                        buy={true}
                        view={true}
                        cancel={false}
                    /> */}
                </CardActions>
            </Card>
        </Grid>
    );
}
  