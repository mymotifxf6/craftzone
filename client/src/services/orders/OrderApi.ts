import { Http } from '../http';
import { getEnvVariables } from '../../environment';

export class OrderApi {
    static fetchOrders() {
        // @ts-ignore
        return Http.get(`${getEnvVariables().base_url}orders`, {baseURL:null});
    }

    static fetchMontlySales() {
        // @ts-ignore
        return Http.get(`${getEnvVariables().base_url}orders/monthsales`, {baseURL:null});
    }
}