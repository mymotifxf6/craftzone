import { OrderActions } from '../../redux/actions/OrderActions';
import { OrderApi } from './OrderApi';

export class OrderApiRepository {
    static fetchOrders() {
        return async (dispatch: any) => {
            try {
                const data = await OrderApi.fetchOrders();
                if (data !== "" && data.data !== "" && typeof data.data !== "undefined") {
                    dispatch(OrderActions.fetchOrderAction(data.data));
                }
                return data;
            } catch (e) {
                return Promise.reject(e);
            }
        }
    }

    static fetchMonthlySales() {
        return async (dispatch: any) => {
            try {
                const data = await OrderApi.fetchMontlySales();
                if (data !== "" && data.data !== "" && typeof data.data !== "undefined") {
                    dispatch(OrderActions.fetchMonthlySalesAction(data.data));
                }
                return data;
            } catch (e) {
                return Promise.reject(e);
            }
        }
    }
    
}