import { UserActions } from '../../redux/actions/UserActions'
import { UserApi } from './UserApi';

export class UserApiRepository {
    static signIn(postData: any) {
        return async (dispatch: any) => {
            try {
                dispatch(UserActions.userSigninRequest());
                const data = await UserApi.signIn(postData);
                if (data !== "" && data.data !== "" && typeof data.data !== "undefined") {                    
                    dispatch(UserActions.userSigninSuccess(data));
                } else {
                    dispatch(UserActions.userSigninFail(data));                   
                }
                return data;
            } catch (e) {
                return Promise.reject(e);
            }
        }
    }

    static createUsers(postData: any) {
        return async(dispatch: any) => {
            try {
                dispatch(UserActions.userRegisterStartsAction());
                const data = await UserApi.createUsers(postData);                
                if (data !== "" && data.data !== "" && typeof data.data !== "undefined") {
                    dispatch(UserActions.userRegisterEndAction(data.data[0].name));
                }       
                return data;
            } catch (e) {
                return Promise.reject(e);
            }
        }
    }

    static signOut() {
        return async(dispatch: any) => {
            try {                
                dispatch(UserActions.userSignoutRequest());                
                setTimeout(() => {
                    dispatch(UserActions.userSignoutSuccess());
                }, 3000);                
            } catch(e) {
                return Promise.reject(e);
            }
        }
    }

    static hideModal() {
        return async(dispatch: any) => {
            try{
                dispatch(UserActions.hideModal());
            } catch(e) {
                return Promise.reject(e);
            }
        }
    }

    static userSigninFailClear() {
        return async(dispatch: any) => {
            try{
                dispatch(UserActions.userSigninFailClear());
            } catch(e) {
                return Promise.reject(e);
            }
        }
    }
}