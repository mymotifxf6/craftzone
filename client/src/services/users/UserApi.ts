import {Http} from '../http';
import {getEnvVariables} from '../../environment';

export class UserApi {
    static signIn(postData: any) {
        // @ts-ignore
        return Http.post(`${getEnvVariables().base_url}users/signin`, postData, {baseURL:null});
    }

    static createUsers(postData: any) {
        // @ts-ignore
        return Http.post(`${getEnvVariables().base_url}users/create`, postData, {baseURL:null});
    }
}