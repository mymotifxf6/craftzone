import { ItemActions } from '../../redux/actions/ItemActions';
import { ItemApi } from './ItemApi';

export class ItemApiRepository {
    static fetchItems() {
        return async (dispatch: any) => {
            try {
                const data = await ItemApi.fetchItems();
                if (data !== "" && data.data !== "" && typeof data.data !== "undefined") {
                    dispatch(ItemActions.fetchItemAction(data.data));
                }
                return data;
            } catch (e) {
                return Promise.reject(e);
            }
        }
    }

    static createItems(postData: any) {
        return async(dispatch: any) => {
            try {
                const data = await ItemApi.createItems(postData);
                if (data !== "" && data.data !== "" && typeof data.data !== "undefined") {
                    dispatch(ItemActions.showToast(data.success));
                }       
                return data;
            } catch (e) {
                return Promise.reject(e);
            }
        }
    }

    static closeSnack() {
        return async (dispatch:any) => {
            try {
                await dispatch(ItemActions.hideToast());
                return;
            } catch (e) {
                console.error(e);
            }
        }
    }
}