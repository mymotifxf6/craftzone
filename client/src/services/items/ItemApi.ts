import {Http} from '../http';
import {getEnvVariables} from '../../environment';

export class ItemApi {
    static fetchItems() {
        // @ts-ignore
        return Http.get(`${getEnvVariables().base_url}items`, {baseURL:null});
    }

    static createItems(postData: any) {
        // @ts-ignore
        return Http.post(`${getEnvVariables().base_url}items/create`, postData, {baseURL:null});
    }
}