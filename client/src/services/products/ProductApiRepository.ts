import { ProductActions } from '../../redux/actions/ProductActions';
import { ProductApi } from './ProductApi';

export class ProductApiRepository {
    static fetchProducts() {
        return async (dispatch: any) => {
            try {
                await dispatch(ProductActions.startFetchingProductAction());
                const data = await ProductApi.fetchProducts();
                if (data !== "" && data.data !== "" && typeof data.data !== "undefined") {                    
                    dispatch(ProductActions.fetchAllProductAction(data.data));                    
                }
                await dispatch(ProductActions.stopFetchingProductAction());
                return data;
            } catch (e) {
                return Promise.reject(e);
            }
        }
    }

    static fetchProduct(id: number) {
        return async (dispatch: any) => {
            try {
                const data = await ProductApi.fetchProduct(id);
                if (data !== "" && data.data !== "" && typeof data.data !== "undefined") {
                    dispatch(ProductActions.fetchSingleProductAction(data.data));
                }
                return data;
            } catch (e) {
                return Promise.reject(e);
            }
        }
    }

    static fetchReviews(id: number) {
        return async (dispatch: any) => {
            try {
                const data = await ProductApi.fetchReviews(id);
                if (data !== "" && data.data !== "" && typeof data.data !== "undefined") {
                    dispatch(ProductActions.fetchProductReviewAction(data.data));
                }
                return data;
            } catch (e) {
                return Promise.reject(e);
            }
        }
    }
}