import {Http} from '../http';
import {getEnvVariables} from '../../environment';

export class ProductApi {
    static fetchProducts() {
        // @ts-ignore
        return Http.get(`${getEnvVariables().base_url}products`, {baseURL:null});
    }

    static fetchProduct(id: number) {
        // @ts-ignore
        return Http.get(`${getEnvVariables().base_url}products/${id}`, {baseURL:null});
    }

    static fetchReviews(id: number) {
        // @ts-ignore
        return Http.get(`${getEnvVariables().base_url}products/reviews/${id}`, {baseURL:null});
    }

}