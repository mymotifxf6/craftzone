import {Http} from '../http';
import {getEnvVariables} from '../../environment';

export class CategoryApi {
    static fetchCategories() {
        // @ts-ignore
        return Http.get(`${getEnvVariables().base_url}categories`, {baseURL:null});
    }

}