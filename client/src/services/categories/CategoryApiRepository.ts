import { CategoryActions } from '../../redux/actions/CategoryActions';
import { CategoryApi } from './CategoryApi';

export class CategoryApiRepository {
    static fetchCategories() {
        return async (dispatch: any) => {
            try {
                const data = await CategoryApi.fetchCategories();
                if (data !== "" && data.data !== "" && typeof data.data !== "undefined") {
                    dispatch(CategoryActions.fetchCategoryActions(data.data));
                }
                return data;
            } catch (e) {
                return Promise.reject(e);
            }
        }
    }
}