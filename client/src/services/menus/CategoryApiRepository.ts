import { CategoryActions } from '../../redux/actions/CategoryActions';
import { CategoryApi } from './CategoryApi';

export class CategoryApiRepository {
    static fetchCategories(postData: any) {
        return async (dispatch: any) => {
            try {
                await dispatch(CategoryActions.startLoadOrderAction());
                const data = await CategoryApi.fetchCategories(postData);                
                if (data !== "" && data.data !== "" && typeof data.data !== "undefined") {
                    dispatch(CategoryActions.fetchCategoryActions(data.data));
                    await dispatch(CategoryActions.stopLoadOrderAction());
                }
                return data;
            } catch (e) {
                return Promise.reject(e);
            }
        }
    }
}