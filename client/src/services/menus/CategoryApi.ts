import {Http} from '../http';
import {getEnvVariables} from '../../environment';

export class CategoryApi {
    static fetchCategories(postData: any) {
        // @ts-ignore
        return Http.get(`${getEnvVariables().base_url}menus?offset=${postData}`, {baseURL:null});
    }
}